package com.example.javadesignpractice.abstractfactoryPattern.builders;

import com.example.javadesignpractice.abstractfactoryPattern.enums.ProductType;
import com.example.javadesignpractice.abstractfactoryPattern.factory.AbstractProductFactory;
import com.example.javadesignpractice.abstractfactoryPattern.factory.impl.FirstProductFactoryImpl;
import com.example.javadesignpractice.abstractfactoryPattern.factory.impl.SecondProductFactoryImpl;
import com.example.javadesignpractice.abstractfactoryPattern.services.ProductService;
import org.springframework.context.annotation.Description;
import org.springframework.stereotype.Component;

@Description(value = "Product builder component")
@Component
public class ProductBuilder {

    public ProductService decidingProduct(ProductType productType) {
        switch (productType) {
            case FIRST_PRODUCT:
                return buildProduct(new FirstProductFactoryImpl());

            case SECOND_PRODUCT:
                return buildProduct(new SecondProductFactoryImpl());
            default:
                throw new IllegalArgumentException();
        }
    }

    private ProductService buildProduct(AbstractProductFactory productFactory) {
        ProductService productService = productFactory.createProduct();
        productService.setTitle("Some title");
        productService.calculate();
        return productService;
    }
}
