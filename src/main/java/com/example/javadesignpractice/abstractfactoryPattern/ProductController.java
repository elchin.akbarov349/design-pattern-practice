package com.example.javadesignpractice.abstractfactoryPattern;

import com.example.javadesignpractice.abstractfactoryPattern.builders.ProductBuilder;
import com.example.javadesignpractice.abstractfactoryPattern.enums.ProductType;
import com.example.javadesignpractice.abstractfactoryPattern.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/product")
public class ProductController {

    private final ProductBuilder productBuilder;

    @PostMapping
    public void createProduct(@RequestParam ProductType type) {
        ProductService productService = productBuilder.decidingProduct(type);
        log.info("Product created: {}", productService);
    }
}
