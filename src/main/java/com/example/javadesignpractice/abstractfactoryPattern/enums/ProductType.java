package com.example.javadesignpractice.abstractfactoryPattern.enums;

public enum ProductType {
    FIRST_PRODUCT,SECOND_PRODUCT
}
