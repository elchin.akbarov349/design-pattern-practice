package com.example.javadesignpractice.abstractfactoryPattern.factory.impl;

import com.example.javadesignpractice.abstractfactoryPattern.factory.AbstractProductFactory;
import com.example.javadesignpractice.abstractfactoryPattern.services.ProductService;
import com.example.javadesignpractice.abstractfactoryPattern.services.impl.FirstProductServiceImpl;
import com.example.javadesignpractice.abstractfactoryPattern.services.impl.SecondProductServiceImpl;
import org.springframework.context.annotation.Description;
import org.springframework.stereotype.Service;

@Description(value = "Concrete implementation of product factory")
@Service
public class SecondProductFactoryImpl implements AbstractProductFactory {
    @Override
    public ProductService createProduct() {
        return new SecondProductServiceImpl();
    }
}
