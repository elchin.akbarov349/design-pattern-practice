package com.example.javadesignpractice.abstractfactoryPattern.factory;

import com.example.javadesignpractice.abstractfactoryPattern.services.ProductService;
import org.springframework.context.annotation.Description;

@Description(value = "Product factory - Creation Factory")
public interface AbstractProductFactory {

    ProductService createProduct();
}
