package com.example.javadesignpractice.abstractfactoryPattern.services.impl;

import com.example.javadesignpractice.abstractfactoryPattern.services.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Description;
import org.springframework.stereotype.Service;

@Slf4j
@Description(value = "SecondProduct implementation")
@Service
public class SecondProductServiceImpl implements ProductService {


    @Override
    public void setTitle(String title) {
        log.info("Setting second product title...");
    }

    @Override
    public void calculate() {
        log.info("Calculating something for second product...");
    }
}
