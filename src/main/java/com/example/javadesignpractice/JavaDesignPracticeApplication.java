package com.example.javadesignpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaDesignPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaDesignPracticeApplication.class, args);
    }

}
