package com.example.javadesignpractice.abstractFactoryPatternV2.util;

import lombok.Data;


@Data
public class ApiRequest<T> {
    private T data;
}
