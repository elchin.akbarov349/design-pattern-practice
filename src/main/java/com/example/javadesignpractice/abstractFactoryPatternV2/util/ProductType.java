package com.example.javadesignpractice.abstractFactoryPatternV2.util;

public enum ProductType {
    MOBILE,COMPUTER
}
