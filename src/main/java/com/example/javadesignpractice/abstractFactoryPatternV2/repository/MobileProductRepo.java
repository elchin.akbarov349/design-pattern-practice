package com.example.javadesignpractice.abstractFactoryPatternV2.repository;

import com.example.javadesignpractice.abstractFactoryPatternV2.entity.MobileProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MobileProductRepo extends JpaRepository<MobileProduct,Long> {
}
