package com.example.javadesignpractice.abstractFactoryPatternV2.repository;

import com.example.javadesignpractice.abstractFactoryPatternV2.entity.ComputerProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComputerProductRepo extends JpaRepository<ComputerProduct,Long> {
}
