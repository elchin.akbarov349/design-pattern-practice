package com.example.javadesignpractice.abstractFactoryPatternV2.entity;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class ComputerProduct extends Product {
    private String computerCode;
    private String computerType;
    private String brandName;
}
