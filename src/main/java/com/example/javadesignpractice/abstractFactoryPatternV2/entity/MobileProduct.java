package com.example.javadesignpractice.abstractFactoryPatternV2.entity;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class MobileProduct extends Product{

    private String mobileCode;
    private String brandName;
}
