package com.example.javadesignpractice.abstractFactoryPatternV2.controller;

import com.example.javadesignpractice.abstractFactoryPatternV2.builder.MyProductBuilder;
import com.example.javadesignpractice.abstractFactoryPatternV2.entity.MobileProduct;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiRequest;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiResponse;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ProductType;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/product/mobile")
public class MobileProductController {

    private final MyProductBuilder myProductBuilder;

    @PostMapping
    public ResponseEntity<ApiResponse> createMobile(@RequestBody MobileProduct product) {
        ApiRequest<MobileProduct> request = new ApiRequest<>();
        request.setData(product);
        return myProductBuilder.createProduct(ProductType.MOBILE, request);
    }

    @GetMapping
    public ResponseEntity<ApiResponse> findAll() {
        return myProductBuilder.findAll(ProductType.MOBILE);
    }
}
