package com.example.javadesignpractice.abstractFactoryPatternV2.controller;

import com.example.javadesignpractice.abstractFactoryPatternV2.builder.MyProductBuilder;
import com.example.javadesignpractice.abstractFactoryPatternV2.entity.ComputerProduct;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiRequest;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiResponse;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ProductType;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/product/computer")
public class ComputerProductController {

    private final MyProductBuilder myProductBuilder;

    @PostMapping
    public ResponseEntity<ApiResponse> createComputer(@RequestBody ComputerProduct product) {
        ApiRequest<ComputerProduct> request = new ApiRequest<>();
        request.setData(product);
        return myProductBuilder.createProduct(ProductType.COMPUTER, request);
    }

    @GetMapping
    public ResponseEntity<ApiResponse> findAll() {
        return myProductBuilder.findAll(ProductType.COMPUTER);
    }
}
