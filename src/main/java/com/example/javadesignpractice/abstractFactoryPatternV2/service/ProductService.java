package com.example.javadesignpractice.abstractFactoryPatternV2.service;

import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiRequest;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiResponse;
import org.springframework.http.ResponseEntity;

public interface ProductService {

    ResponseEntity<ApiResponse> createProduct(ApiRequest request);

    ResponseEntity<ApiResponse> findAll();
}
