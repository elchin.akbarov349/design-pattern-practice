package com.example.javadesignpractice.abstractFactoryPatternV2.service;

import com.example.javadesignpractice.abstractFactoryPatternV2.entity.ComputerProduct;
import com.example.javadesignpractice.abstractFactoryPatternV2.repository.ComputerProductRepo;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiRequest;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiResponse;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.CommonResponseBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "Computer")
@RequiredArgsConstructor
public class ComputerProductServiceImpl implements ProductService {

    @Autowired
    private ComputerProductRepo computerProductRepo;
    @Autowired
    private CommonResponseBuilder commonResponseBuilder;

    @Override
    public ResponseEntity<ApiResponse> createProduct(ApiRequest request) {
        ComputerProduct product = (ComputerProduct) request.getData();
        product = computerProductRepo.save(product);
        return commonResponseBuilder.buildResponse(HttpStatus.CREATED.value(), "Success", product);
    }

    @Override
    public ResponseEntity<ApiResponse> findAll() {
        List<ComputerProduct> computerProducts = computerProductRepo.findAll();
        return commonResponseBuilder.buildResponse(HttpStatus.OK.value(), "Success", computerProducts);
    }
}
