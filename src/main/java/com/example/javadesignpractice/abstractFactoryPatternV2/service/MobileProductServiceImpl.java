package com.example.javadesignpractice.abstractFactoryPatternV2.service;

import com.example.javadesignpractice.abstractFactoryPatternV2.entity.MobileProduct;
import com.example.javadesignpractice.abstractFactoryPatternV2.repository.MobileProductRepo;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiRequest;

import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiResponse;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.CommonResponseBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "Mobile")
@RequiredArgsConstructor
public class MobileProductServiceImpl implements ProductService {

    @Autowired
    private MobileProductRepo mobileProductRepo;
    @Autowired
    private CommonResponseBuilder commonResponseBuilder;

    @Override
    public ResponseEntity<ApiResponse> createProduct(ApiRequest request) {
        MobileProduct product = (MobileProduct) request.getData();
        product = mobileProductRepo.save(product);
        return commonResponseBuilder.buildResponse(HttpStatus.CREATED.value(), "Success", product);
    }

    @Override
    public ResponseEntity<ApiResponse> findAll() {
        List<MobileProduct> mobileProductList = mobileProductRepo.findAll();
        return commonResponseBuilder.buildResponse(HttpStatus.OK.value(), "Success", mobileProductList);
    }
}
