package com.example.javadesignpractice.abstractFactoryPatternV2.factory.impl;

import com.example.javadesignpractice.abstractFactoryPatternV2.factory.AbstractProductFactory;
import com.example.javadesignpractice.abstractFactoryPatternV2.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service(value = "MobileFactory")
@RequiredArgsConstructor
public class MobileProductFactoryImpl implements AbstractProductFactory {

    @Qualifier(value = "Mobile")
    private final ProductService mobileService;

    @Override
    public ProductService createService() {
        return mobileService;
    }
}
