package com.example.javadesignpractice.abstractFactoryPatternV2.factory;

import com.example.javadesignpractice.abstractFactoryPatternV2.service.ProductService;

public interface AbstractProductFactory {

    ProductService createService();
}
