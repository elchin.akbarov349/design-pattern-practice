package com.example.javadesignpractice.abstractFactoryPatternV2.factory.impl;

import com.example.javadesignpractice.abstractFactoryPatternV2.factory.AbstractProductFactory;
import com.example.javadesignpractice.abstractFactoryPatternV2.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service(value = "ComputerFactory")
@RequiredArgsConstructor
public class ComputerProductFactoryImpl implements AbstractProductFactory {

    @Qualifier(value = "Computer")
    private final ProductService computerService;

    @Override
    public ProductService createService() {
        return computerService;
    }
}
