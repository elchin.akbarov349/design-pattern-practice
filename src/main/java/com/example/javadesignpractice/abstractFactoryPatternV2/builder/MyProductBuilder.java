package com.example.javadesignpractice.abstractFactoryPatternV2.builder;

import com.example.javadesignpractice.abstractFactoryPatternV2.factory.AbstractProductFactory;
import com.example.javadesignpractice.abstractFactoryPatternV2.factory.impl.ComputerProductFactoryImpl;
import com.example.javadesignpractice.abstractFactoryPatternV2.factory.impl.MobileProductFactoryImpl;
import com.example.javadesignpractice.abstractFactoryPatternV2.service.ProductService;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiRequest;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ApiResponse;
import com.example.javadesignpractice.abstractFactoryPatternV2.util.ProductType;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MyProductBuilder {

    @Qualifier(value = "MobileFactory")
    private final AbstractProductFactory mobileFactory;
    @Qualifier(value = "ComputerFactory")
    private final AbstractProductFactory computerFactory;


    public ResponseEntity<ApiResponse> createProduct(ProductType productType, ApiRequest request) {
        ProductService productService = createService(productType);
        return productService.createProduct(request);
    }

    public ResponseEntity<ApiResponse> findAll(ProductType productType) {
        ProductService productService = createService(productType);
        return productService.findAll();
    }

    private ProductService createService(ProductType type) {
        switch (type) {
            case MOBILE:
                return mobileFactory.createService();
            case COMPUTER:
                return computerFactory.createService();
            default:
                throw new IllegalArgumentException();
        }
    }
}
