package com.example.javadesignpractice.builderPattern.controller;

import com.example.javadesignpractice.builderPattern.service.CustomerService;
import com.example.javadesignpractice.builderPattern.util.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping
    public ResponseEntity<ApiResponse> getEmployees() {
        return customerService.findAll();
    }
}
