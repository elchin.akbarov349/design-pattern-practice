package com.example.javadesignpractice.builderPattern.service;

import com.example.javadesignpractice.builderPattern.entity.Customer;
import com.example.javadesignpractice.builderPattern.repository.CustomerRepository;
import com.example.javadesignpractice.builderPattern.util.ApiResponse;
import com.example.javadesignpractice.builderPattern.util.ResponseBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final ResponseBuilder responseBuilder;

    public ResponseEntity<ApiResponse> findAll() {
        List<Customer> customers = customerRepository.findAll();
        return responseBuilder.buildResponse(HttpStatus.OK.value(),
                "Customer details", customers);
    }
}
