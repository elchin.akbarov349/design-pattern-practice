package com.example.javadesignpractice.factoryPattern.service;

import com.example.javadesignpractice.factoryPattern.viewer.ViewerFactory;
import com.example.javadesignpractice.factoryPattern.viewer.ViewerType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ViewerService {

    private final ViewerFactory viewerFactory;

    public void view(ViewerType viewerType, Object o) {
        viewerFactory.getViewer(viewerType).view(o);
    }
}
