package com.example.javadesignpractice.factoryPattern.viewer;

public enum ViewerType {

    DOCUMENT,IMAGE,VIDEO
}
