package com.example.javadesignpractice.factoryPattern.viewer;

import com.example.javadesignpractice.factoryPattern.model.Video;
import org.springframework.stereotype.Component;

@Component
public class VideoViewer implements Viewer<Video> {

    private final static ViewerType VIEWER_TYPE = ViewerType.VIDEO;

    @Override
    public ViewerType getType() {
        return VIEWER_TYPE;
    }

    @Override
    public void view(Video object) {
        System.out.println("Viewing video");
    }
}
