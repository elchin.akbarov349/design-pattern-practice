package com.example.javadesignpractice.factoryPattern.viewer;

import com.example.javadesignpractice.factoryPattern.model.Document;
import org.springframework.stereotype.Component;

@Component
public class DocumentViewer implements Viewer<Document> {

    private static final ViewerType VIEWER_TYPE = ViewerType.DOCUMENT;

    @Override
    public ViewerType getType() {
        return VIEWER_TYPE;
    }

    @Override
    public void view(Document object) {
        System.out.println("Viewing document");
    }
}
