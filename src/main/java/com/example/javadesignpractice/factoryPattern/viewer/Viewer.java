package com.example.javadesignpractice.factoryPattern.viewer;

public interface Viewer<T> {
    ViewerType getType();

    void view(T object);
}
