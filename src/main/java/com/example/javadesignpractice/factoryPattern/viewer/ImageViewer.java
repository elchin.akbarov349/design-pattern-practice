package com.example.javadesignpractice.factoryPattern.viewer;

import com.example.javadesignpractice.factoryPattern.model.Image;
import org.springframework.stereotype.Component;

@Component
public class ImageViewer implements Viewer<Image> {

    private static final ViewerType VIEWER_TYPE = ViewerType.IMAGE;

    @Override
    public ViewerType getType() {
        return VIEWER_TYPE;
    }

    @Override
    public void view(Image object) {
        System.out.println("Viewing image");
    }
}
